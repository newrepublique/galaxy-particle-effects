An experiment to recreate a nice particle effect we've seen on a few sites across Dribbble and the wide world of the internet.

View latest build here: [http://push.newrepublique.com/experiments/particles](http://push.newrepublique.com/experiments/particles)